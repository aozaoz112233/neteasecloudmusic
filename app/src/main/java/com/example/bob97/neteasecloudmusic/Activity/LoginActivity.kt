package com.example.bob97.neteasecloudmusic.Activity

import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.text.method.TransformationMethod
import android.util.Log
import android.view.Window
import android.view.WindowManager
import com.example.bob97.neteasecloudmusic.Interface.LoginService
import com.example.bob97.neteasecloudmusic.networkManager.Net
import com.example.bob97.neteasecloudmusic.user.User
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by bob97 on 2017/12/30.
 */
class LoginActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        User.get().mContext = applicationContext
        verticalLayout{
            backgroundColor = Color.WHITE
            val phone = editText(){
                hint = "手机号"
                setText(User.get().phone)
            }
            val password = editText(){
                hint = "密码"
                inputType = InputType.TYPE_CLASS_TEXT
                inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
                this.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            button("登录"){
                onClick {
//                    User.get().login(
//                            phone.text.toString(),
//                            password.text.toString()
//                    ){
//                        error ->
//                        if (error == null)
//                        {
//                            startActivity(intentFor<MainActivity>().clearTask().newTask())
//                        }
//                        else{
//                            toast("登录失败")
//                        }
//                    }
                    Net.getInstance()
                            .retrofit
                            .create(LoginService::class.java)
                            .getLoginUser(phone.text.toString(),password.text.toString())
                            .subscribeOn(Schedulers.io())
                            .unsubscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                User ->
                                if (User != null){
                                    startActivity(intentFor<MainActivity>().clearTask().newTask())
                                }
                                else{
                                    toast("登录失败")
                                }
                            })
                }
            }
            button("清除用户信息") {
                onClick {
               //     User.get().restInfo()
                    toast("成功")
                }
            }
        }
    }
}