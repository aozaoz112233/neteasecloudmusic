package com.example.bob97.neteasecloudmusic.Activity

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.bob97.neteasecloudmusic.BlurImageQcl.FastBlurUtil
import com.example.bob97.neteasecloudmusic.R
import com.example.bob97.neteasecloudmusic.data.Track
import com.example.bob97.neteasecloudmusic.networkManager.NetWorkManager
import kotlinx.android.synthetic.main.fragment_rank.*
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.*

/**
 * Created by bob97 on 2018/1/5.
 */
class MusicPlayerActivity:AppCompatActivity (){
    val player = MediaPlayer()
    var musicList: ArrayList<Track>? = null
    var idx = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idx = intent.extras["idx"] as Int
        musicList = intent.extras["list"] as ArrayList<Track>
        frameLayout {
            imageView() {
                id = R.id.MUSIC_BG
            }.lparams(matchParent, matchParent)

            verticalLayout {
                textView() {
                    id = R.id.MUSIC_Title
                    textSize = 30f
                    textColor = Color.WHITE
                }.lparams() {
                    gravity = Gravity.CENTER_HORIZONTAL
                }
                textView() {
                    id = R.id.MUSIC_SubTitle
                    textSize = 24f
                    textColor = Color.WHITE
                }.lparams() {
                    gravity = Gravity.CENTER_HORIZONTAL
                }
                imageView() {
                    id = R.id.MUSIC_Img
                }.lparams(dip(200), dip(200)) {
                    gravity = Gravity.CENTER_HORIZONTAL
                    topMargin = dip(25)
                }
                horizontalProgressBar {
                    max = 100
                    id = R.id.MUSIC_Progress
                }.lparams(width = matchParent) {
                    topMargin = dip(25)
                }
            }

            linearLayout {
                imageView {
                    id = R.id.MUSIC_MODEL
                    setImageResource(R.drawable.play_icn_loop_prs)
                }.lparams() {
                    width = wrapContent
                    height = wrapContent
                    gravity = Gravity.CENTER_VERTICAL
                    weight = 1f
                }

                imageView {
                    id = R.id.MUSIC_LAST
                    setImageResource(R.drawable.play_btn_prev)
                    onClick {
                         prev()
                    }

                }.lparams() {
                    width = wrapContent
                    height = wrapContent
                    gravity = Gravity.CENTER_VERTICAL
                    weight = 1f
                }

                imageView {
                    id = R.id.MUSIC_PLAY
                    setImageResource(R.drawable.lock_btn_pause)
                    onClick {
                            if (player.isPlaying){
                                player.pause()
                                setImageResource(R.drawable.play_rdi_btn_play)
                            }else{
                                player.start()
                                setImageResource(R.drawable.lock_btn_pause)
                            }
                }
            }.lparams() {
                width = wrapContent
                height = wrapContent
                gravity = Gravity.CENTER_VERTICAL
                weight = 1f
            }

            imageView {
                id = R.id.MUSIC_NEXT
                setImageResource(R.drawable.play_btn_next_prs)
                onClick {
                    next()
                }
            }.lparams() {
                width = wrapContent
                height = wrapContent
                gravity = Gravity.CENTER_VERTICAL
                weight = 1f
            }

            imageView {
                id = R.id.MUSIC_PLAYLIST
                setImageResource(R.drawable.play_icn_src_prs)
            }.lparams() {
                width = wrapContent
                height = wrapContent
                gravity = Gravity.CENTER_VERTICAL
                weight = 1f
            }
        }.lparams(width = matchParent, height = matchParent) {
            topMargin = dip(250)
        }
    }


        player.setOnBufferingUpdateListener{ mediaPlayer, i ->
            find<ProgressBar>(R.id.MUSIC_Progress).secondaryProgress = i
            toast("加载进度 $i")
        }
        player.setOnCompletionListener {
            next()
        }
        refreshUI()
        playById(musicList!![idx].id!!.toString())
    }

    //定时任务来更新进度条
    val handler = Handler()
    var task:Runnable? = (object : Runnable{
        override fun run() {
            if (player.isPlaying) {
                find<ProgressBar>(R.id.MUSIC_Progress).progress = (player.currentPosition / player.duration) * 100
            }
            handler.postDelayed(this,200)
        }
    })
    fun startTimer(){
        val handle = Handler()
        handle.post(task)
    }

    //播放音乐
    fun play(){
        if (!player.isPlaying){
            player.start()
        }else{
            refreshUI()
            playById(musicList!![idx].id!!.toString())
        }
    }
    //暂停播放
    fun pause(){
        player.pause()
    }
    //下一首
    fun next(){
        if (idx == musicList!!.size - 1) idx = 0
        else idx++
        play()
    }
    //前一首
    fun prev(){
        if(idx == 0) idx = musicList!!.size
        else idx--
        play()
    }

    //刷新UI界面
    fun refreshUI(){
        //获取控件
        val bg = find<ImageView>(R.id.MUSIC_BG)
        val img = find<ImageView>(R.id.MUSIC_Img)
        val title = find<TextView>(R.id.MUSIC_Title)
        val subTitle = find<TextView>(R.id.MUSIC_SubTitle)
        val item = musicList!![idx]
        //设置标题
        title.text = item.name
        subTitle.text = item.ar[0].name
        //设置背景处理
        val target = (object : SimpleTarget<Bitmap>(){
            override fun onResourceReady(drawable: Bitmap?, glideAnimation: Transition<in Bitmap>?) {
                val blurBitmap = FastBlurUtil().toBlur(drawable!!,8)
                bg.scaleType = ImageView.ScaleType.CENTER_CROP
                bg.setImageBitmap(blurBitmap)
            }
        })
        //加载网络图片
//        Glide.with(this)
//                .load(item.al.picUrl)
//               .placeholder(R.mipmap.ic_launcher)
//                .error(R.mipmap.ic_launcher_round)
//                .centerCrop()
//                .into(target)
//        Glide.with(this)
//                .load(item.al.picUrl)
//                .placeholder(R.mipmap.ic_launcher)
//                .error(R.mipmap.ic_launcher_round)
//               .centerCrop()
//                .into(img)

    }

    //根据ID播放音乐
    fun playById(idx: String){
        fun autoPlaySong(url: String){
            player.stop()
            player.reset()
            player.setDataSource(this, Uri.parse(url))
            player.prepareAsync()
            player.setOnPreparedListener{mediaPlayer ->
                mediaPlayer.start()
            }
        }
        autoPlaySong(NetWorkManager.findMusicById(idx))
    }
    //释放MediaPlayer占用的资源
    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(task)
        player.stop()
        player.release()
    }
    }
