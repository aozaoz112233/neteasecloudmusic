package com.example.bob97.neteasecloudmusic.Activity

import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.Window
import android.view.WindowManager
import com.example.bob97.neteasecloudmusic.Adapter.TracksListAdapter
import com.example.bob97.neteasecloudmusic.data.RankResult
import com.example.bob97.neteasecloudmusic.data.Track
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.json.JSONObject



/**
 * Created by bob97 on 2018/1/2.
 */
class TracksListActivity:AppCompatActivity() {
    var tracks:List<Track>? = null
    var rankResult:RankResult? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //设置全屏，去掉toolbar的自带导航
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        //获取标题
       rankResult = intent.extras["data"] as RankResult
        tracks = rankResult?.playlist?.tracks
        val titleStr = rankResult?.playlist?.creator?.nickname
        val subTitleStr = rankResult?.playlist?.creator?.signature
        //UI布局界面
        verticalLayout {
            //设置工具条
            toolbar {
                backgroundColor = Color.DKGRAY
                title = titleStr
                subtitle = subTitleStr
                setTitleTextColor(Color.WHITE)
                setSubtitleTextColor(Color.WHITE)
            }
            //设置列表
            recyclerView {
                val myLayoutManager = LinearLayoutManager(context)
                myLayoutManager.orientation = OrientationHelper.VERTICAL
                layoutManager = myLayoutManager
                //设置Adapter
            adapter = TracksListAdapter(tracks!!,{
                idx -> startActivity<MusicPlayerActivity>("idx" to idx,"list" to this@TracksListActivity.tracks!!)
            })
                //设置动画
                itemAnimator = DefaultItemAnimator()
                //设置分隔
                addItemDecoration(DividerItemDecoration(this@TracksListActivity, LinearLayoutManager.VERTICAL))
                adapter.notifyDataSetChanged()

            }.lparams(matchParent, matchParent)
        }


    }
}