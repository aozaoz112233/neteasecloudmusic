package com.example.bob97.neteasecloudmusic.Adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.bob97.neteasecloudmusic.Interface.GetRankService
import com.example.bob97.neteasecloudmusic.R
import com.example.bob97.neteasecloudmusic.data.RankResult
import com.example.bob97.neteasecloudmusic.networkManager.Net
import com.example.bob97.neteasecloudmusic.networkManager.NetWorkManager
import kotlinx.android.synthetic.main.activity_main.view.*
import org.jetbrains.anko.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by bob97 on 2018/1/1.
 */
class RankRecycleAdapter(internal val didSelectAtPos:(data:RankResult)-> Unit)
    :RecyclerView.Adapter<RankRecycleAdapter.ViewHolder>(){
    private var mItems = mutableListOf<RankResult>()
    internal var mContext:Context? =null
    private val maxRankId = 21
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        mContext = parent?.context
        return ViewHolder(RankItemUI().createView(AnkoContext.create(parent.context,parent)))

    }
    //绑定ViewHolder与数据
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        fun bind(model: RankResult){
            holder.title.text = model.playlist.creator.nickname
                    Glide.with(mContext)
                            .load(model.playlist.creator.avatarUrl)
                            .into(holder.img)
            with(holder.container){
                setOnClickListener(object :View.OnClickListener{
                    override fun onClick(v: View?) {
                        didSelectAtPos(mItems[position])
                    }
                })
            }
        }
        val item = mItems[position]
        bind(item)
    }

    //返回Item的数量
    override fun getItemCount(): Int {
        return mItems.size
    }
    //加载数据，从idx为3分批获取详细数据
    internal fun loadList(complete:(()-> Unit)){
        loadChannel(3)
        complete()
    }
    //加载id为idx的channel
    fun loadChannel(idx: Int){
        if (idx < maxRankId){
//            return NetWorkManager.getRank("$idx"){data,err->
//                if (err == null){
//                    mItems.add(data!!)
//                    notifyDataSetChanged()
//                }
//                else mContext!!.toast("$err")
//                loadChannel(idx + 1)
            Net.getInstance()
                    .retrofit
                    .create(GetRankService::class.java)
                    .getRank(idx.toString())
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        RankResult->
                        if (RankResult != null) {
                            mItems.add(RankResult)
                            notifyDataSetChanged()
                            loadChannel(idx + 1)
                        }
                    })
            }
    }
    //创建ViewHolder
    class ViewHolder(view: View):RecyclerView.ViewHolder(view){
        var container = view.find<FrameLayout>(R.id.RankListItem)
        var img = view.find<ImageView>(R.id.RankListItem_img)
        var title = view.find<TextView>(R.id.RankListItem_title)
    }
    //单元格布局
    class RankItemUI:AnkoComponent<ViewGroup>{
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui){
                frameLayout{
                    padding = 10
                    backgroundColor = Color.LTGRAY
                    lparams(width = matchParent,height = wrapContent)
                    frameLayout{
                        id = R.id.RankListItem
                        imageView{
                            backgroundColor = Color.GRAY
                            id = R.id.RankListItem_img
                        }.lparams(width = matchParent,height = matchParent)
                        textView("Title"){
                            textColor = Color.WHITE
                            textSize = 14f
                            id = R.id.RankListItem_title
                        }.lparams(width = matchParent, height = matchParent){
                            topMargin = dip(0)
                        }

                    }.lparams(width = dip(180),height = dip(180))
                }
            }
        }
    }

}