package com.example.bob97.neteasecloudmusic.Adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.bob97.neteasecloudmusic.GlideModule
import com.example.bob97.neteasecloudmusic.R
import com.example.bob97.neteasecloudmusic.data.Track
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Created by bob97 on 2018/1/2.
 */
class TracksListAdapter(val mItems:List<Track>,internal val didSelectAtPos:(idx:Int)-> Unit)
    : RecyclerView.Adapter<TracksListAdapter.ViewHolder>() {
    internal var mContext:Context? = null

    //创建ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        return ViewHolder(TrackItemUI().createView(AnkoContext.create(parent.context,parent)))
    }
    //绑定ViewHolder与数据
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        fun bind(model:Track){
            holder.title.text = model.name
            holder.subTitle.text = model.ar[0].name + "-" +model.al.name
            holder.no.text = "$position"
            glide.with(mContext)
                    .load(model.al.picUrl)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher_round)
                    .dontAnimate()
                    .centerCrop()
                    .into(holder.img)

            //绑定点击事件
            with(holder.container){
                setOnClickListener(object :View.OnClickListener{
                    override fun onClick(p0: View?) {
                        didSelectAtPos(position)
                    }
                })
            }

        }
        val item = mItems!![position]
        bind(item)
    }
    //返回Item的个数
    override fun getItemCount(): Int {
        return mItems.size
    }

    //定义ViewHolder类
    class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        var container = view.find<LinearLayout>(R.id.TracksListItem)
        var img = view.find<ImageView>(R.id.TracksListItem_img)
        var title = view.find<TextView>(R.id.TracksListItem_title)
        var subTitle = view.find<TextView>(R.id.TracksListItem_subTitle)
        var no = view.find<TextView>(R.id.TracksListItem_no)
    }
    //Item的UI绘制
    class TrackItemUI: AnkoComponent<ViewGroup>{
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui){
                linearLayout{
                    id = R.id.TracksListItem
                    textView("01"){
                        textColor = Color.DKGRAY
                        textSize = 30F
                        id = R.id.TracksListItem_no
                    }.lparams(width = dip(40),height = dip(40))
                    imageView{
                        backgroundColor = Color.GRAY
                        id = R.id.TracksListItem_img
                    }.lparams(width = dip(70),height = dip(70))
                    verticalLayout{
                        textView("Title"){
                            textColor = Color.GRAY
                            textSize = 20f
                            id = R.id.TracksListItem_title
                        }
                        textView("SubTitle"){
                            textColor = Color.BLACK
                            textSize = 14f
                            id = R.id.TracksListItem_subTitle
                        }
                    }
                }
            }
        }
    }

}