package com.example.bob97.neteasecloudmusic.Fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import com.example.bob97.neteasecloudmusic.Activity.MainActivity
import com.example.bob97.neteasecloudmusic.Activity.TracksListActivity
import com.example.bob97.neteasecloudmusic.Adapter.RankRecycleAdapter
import com.example.bob97.neteasecloudmusic.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_rank.*
import org.jetbrains.anko.singleTop
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.onUiThread
import org.jetbrains.anko.support.v4.toast

class RankFragment : Fragment() {
    var isLoading = false
//    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        val rootView = inflater!!.inflate(R.layout.fragment_rank,container,false)
//        return rootView
//    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //设置rankRecyclerView布局管理器
        rankRecyclerView!!.layoutManager = GridLayoutManager(context,2)
        val adapter = RankRecycleAdapter(
                { data ->
                    context!!.startActivity<TracksListActivity>("data" to data)
                })
        //设置adapter

        rankRecyclerView!!.adapter = adapter
        adapter.mContext = this.context
        //设置动画
        rankRecyclerView!!.itemAnimator = DefaultItemAnimator()

        //设置SwipeRefreshLayout颜色
        swipe_container.setColorSchemeResources(android.R.color.holo_blue_light, android.R.color.holo_red_light,
                android.R.color.holo_orange_light,android.R.color.holo_green_light)
        //设置SwipeRefreshLayout的刷新事件
        swipe_container.onRefresh {
            if (!isLoading){
                isLoading = true
                adapter.loadList({
                 swipe_container.isRefreshing = false
                    isLoading = false
                })
            }
            else toast("正在加载ing")
        }
        //开始刷新
        swipe_container.post{
            onUiThread {
                swipe_container.isRefreshing = true
            }
        }
        //自动刷新一次，读取一次数据
        adapter.loadList({
            swipe_container.post{
                onUiThread {
                    swipe_container.isRefreshing = false
                }
            }
        })
    }
}

