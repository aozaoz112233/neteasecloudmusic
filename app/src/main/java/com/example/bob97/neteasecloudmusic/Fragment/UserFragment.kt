package com.example.bob97.neteasecloudmusic.Fragment


import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.example.bob97.neteasecloudmusic.BlurImageQcl.FastBlurUtil

import com.example.bob97.neteasecloudmusic.R
import com.example.bob97.neteasecloudmusic.user.User
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.UI


/**
 * A simple [Fragment] subclass.
 */
class UserFragment : Fragment() {
    var image:ImageView? = null
    var bg:ImageView? = null

     fun onCreat(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return UI {
            frameLayout {
                bg = imageView().lparams(matchParent, matchParent)
                linearLayout {
                    image = imageView(){}.lparams(dip(120),dip(120))
                    verticalLayout {
                        textView (User.get().nickName){
                            textSize = 30f
                            textColor = Color.BLACK
                        }
                        textView (User.get().name){
                            textSize = 20f
                            textColor = Color.BLACK
                        }

                    }
                }
            }
        }.view
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        Glide.with(this)
//                .load(User.get().avatarUrl)
//                .placeholder(R.mipmap.ic_launcher)
//                .error(R.mipmap.ic_launcher_round)
//                .dontAnimate()
//                .centerCrop()
//                .into(image)

//        val target = (object : SimpleTarget<Bitmap>(){
//            override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
//                val blurBitmap = FastBlurUtil().toBlur(resource!!,8)
//                bg!!.scaleType = ImageView.ScaleType.CENTER_CROP
//                bg!!.setImageBitmap(blurBitmap)
//            }
//        })

//        Glide.with(this)
//                .load(User.get().avatarUrl)
//                .asBitmap()
//                .placeholder(R.mipmap.ic_launcher)
//                .error(R.mipmap.ic_launcher_round)
//                .dontAnimate()
//                .centerCrop()
//                .into(target)
    }

}// Required empty public constructor
