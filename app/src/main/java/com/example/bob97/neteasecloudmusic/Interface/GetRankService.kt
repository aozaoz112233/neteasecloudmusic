package com.example.bob97.neteasecloudmusic.Interface

import com.example.bob97.neteasecloudmusic.data.RankResult
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * Created by bob97 on 2018/7/19.
 */
interface GetRankService {
    @GET("/top/list")
    fun getRank(@Query("idx") idx:String):Observable<RankResult>
}