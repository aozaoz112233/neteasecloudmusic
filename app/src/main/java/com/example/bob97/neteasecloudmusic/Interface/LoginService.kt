package com.example.bob97.neteasecloudmusic.Interface

import com.example.bob97.neteasecloudmusic.user.User
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * Created by bob97 on 2018/7/18.
 */
interface LoginService{
    @GET("/login/cellphone")
    fun getLoginUser(@Query("phone") phone:String, @Query("password") passowrd:String): Observable<User>
}