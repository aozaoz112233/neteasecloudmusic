package com.example.bob97.neteasecloudmusic.data

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by bob97 on 2017/12/28.
 */

data class accounts(
		@SerializedName("loginType") var loginType: Int,
		@SerializedName("code") var code: Int,
		@SerializedName("account") var account: Account,
		@SerializedName("profile") var profile: Profile,
		@SerializedName("bindings") var bindings: List<Binding>
) {
    class Deserializer : ResponseDeserializable<accounts> {
    override fun deserialize(content: String) = Gson().fromJson(content, accounts::class.java)
    }
}

data class Profile(
		@SerializedName("description") var description: String,
		@SerializedName("detailDescription") var detailDescription: String,
		@SerializedName("djStatus") var djStatus: Int,
		@SerializedName("followed") var followed: Boolean,
		@SerializedName("userType") var userType: Int,
		@SerializedName("authStatus") var authStatus: Int,
		@SerializedName("backgroundUrl") var backgroundUrl: String,
		@SerializedName("backgroundImgIdStr") var backgroundImgIdStr: String,
		@SerializedName("avatarImgIdStr") var avatarImgIdStr: String,
		@SerializedName("defaultAvatar") var defaultAvatar: Boolean,
		@SerializedName("avatarUrl") var avatarUrl: String,
		@SerializedName("gender") var gender: Int,
		@SerializedName("birthday") var birthday: Long,
		@SerializedName("city") var city: Int,
		@SerializedName("mutual") var mutual: Boolean,
		@SerializedName("remarkName") var remarkName: Any,
		@SerializedName("expertTags") var expertTags: Any,
		@SerializedName("avatarImgId") var avatarImgId: Long,
		@SerializedName("backgroundImgId") var backgroundImgId: Long,
		@SerializedName("userId") var userId: Int,
		@SerializedName("nickname") var nickname: String,
		@SerializedName("vipType") var vipType: Int,
		@SerializedName("accountStatus") var accountStatus: Int,
		@SerializedName("province") var province: Int,
		@SerializedName("signature") var signature: String,
		@SerializedName("authority") var authority: Int
)


data class Binding(
		@SerializedName("url") var url: String,
		@SerializedName("expiresIn") var expiresIn: Int,
		@SerializedName("refreshTime") var refreshTime: Int,
		@SerializedName("tokenJsonStr") var tokenJsonStr: String,
		@SerializedName("userId") var userId: Int,
		@SerializedName("expired") var expired: Boolean,
		@SerializedName("id") var id: Long,
		@SerializedName("type") var type: Int
)

data class Account(
		@SerializedName("id") var id: Int,
		@SerializedName("userName") var userName: String,
		@SerializedName("type") var type: Int,
		@SerializedName("status") var status: Int,
		@SerializedName("whitelistAuthority") var whitelistAuthority: Int,
		@SerializedName("createTime") var createTime: Long,
		@SerializedName("salt") var salt: String,
		@SerializedName("tokenVersion") var tokenVersion: Int,
		@SerializedName("ban") var ban: Int,
		@SerializedName("baoyueVersion") var baoyueVersion: Int,
		@SerializedName("donateVersion") var donateVersion: Int,
		@SerializedName("vipType") var vipType: Int,
		@SerializedName("viptypeVersion") var viptypeVersion: Long,
		@SerializedName("anonimousUser") var anonimousUser: Boolean
)





