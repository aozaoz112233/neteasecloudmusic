package com.example.bob97.neteasecloudmusic.data

import android.os.Parcel
import android.os.Parcelable
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by bob97 on 2018/1/1.
 */


data class RankResult(
		@SerializedName("playlist") var playlist: Playlist,
		@SerializedName("code") var code: Int,
		@SerializedName("privileges") var privileges: List<Privilege>
) : Parcelable {
	class Deserializer : ResponseDeserializable<RankResult> {
		override fun deserialize(content: String) = Gson().fromJson(content, RankResult::class.java)
	}

	constructor(source: Parcel) : this(
			source.readParcelable<Playlist>(Playlist.javaClass.classLoader),
			source.readInt(),
			source.createTypedArrayList(Privilege.CREATOR)
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeParcelable(playlist,flags)
		writeInt(code)
		writeTypedList(privileges)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<RankResult> = object : Parcelable.Creator<RankResult> {
			override fun createFromParcel(source: Parcel): RankResult = RankResult(source)
			override fun newArray(size: Int): Array<RankResult?> = arrayOfNulls(size)
		}
	}
}

data class Playlist(
		@SerializedName("subscribers") var subscribers: List<Any>,
		@SerializedName("subscribed") var subscribed: Boolean,
		@SerializedName("creator") var creator: Creator,
		@SerializedName("tracks") var tracks: List<Track>,
		//@SerializedName("trackIds") var trackIds: List<TrackId>,
		@SerializedName("ordered") var ordered: Boolean,
		@SerializedName("tags") var tags: List<String>,
		@SerializedName("adType") var adType: Int,
		@SerializedName("trackNumberUpdateTime") var trackNumberUpdateTime: Long,
		@SerializedName("subscribedCount") var subscribedCount: Int,
		@SerializedName("cloudTrackCount") var cloudTrackCount: Int,
		@SerializedName("status") var status: Int,
		@SerializedName("description") var description: String,
		@SerializedName("newImported") var newImported: Boolean,
		@SerializedName("userId") var userId: Int,
		@SerializedName("specialType") var specialType: Int,
		@SerializedName("privacy") var privacy: Int,
		@SerializedName("coverImgId") var coverImgId: Long,
		@SerializedName("createTime") var createTime: Long,
		@SerializedName("updateTime") var updateTime: Long,
		@SerializedName("trackUpdateTime") var trackUpdateTime: Long,
		@SerializedName("trackCount") var trackCount: Int,
		@SerializedName("commentThreadId") var commentThreadId: String,
		@SerializedName("highQuality") var highQuality: Boolean,
		@SerializedName("playCount") var playCount: Int,
		@SerializedName("coverImgUrl") var coverImgUrl: String,
		@SerializedName("name") var name: String,
		@SerializedName("id") var id: Int,
		@SerializedName("shareCount") var shareCount: Int,
		@SerializedName("coverImgId_str") var coverImgIdStr: String,
		@SerializedName("commentCount") var commentCount: Int
) : Parcelable {
	constructor(source: Parcel) : this(
			ArrayList<Any>().apply { source.readList(this, Any::class.java.classLoader) },
			1 == source.readInt(),
			source.readParcelable<Creator>(Creator::class.java.classLoader),
			source.createTypedArrayList(Track.CREATOR),
			1 == source.readInt(),
			source.createStringArrayList(),
			source.readInt(),
			source.readLong(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readString(),
			1 == source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readLong(),
			source.readLong(),
			source.readLong(),
			source.readLong(),
			source.readInt(),
			source.readString(),
			1 == source.readInt(),
			source.readInt(),
			source.readString(),
			source.readString(),
			source.readInt(),
			source.readInt(),
			source.readString(),
			source.readInt()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeList(subscribers)
		writeInt((if (subscribed) 1 else 0))
		writeParcelable(creator, 0)
		writeTypedList(tracks)
		writeInt((if (ordered) 1 else 0))
		writeStringList(tags)
		writeInt(adType)
		writeLong(trackNumberUpdateTime)
		writeInt(subscribedCount)
		writeInt(cloudTrackCount)
		writeInt(status)
		writeString(description)
		writeInt((if (newImported) 1 else 0))
		writeInt(userId)
		writeInt(specialType)
		writeInt(privacy)
		writeLong(coverImgId)
		writeLong(createTime)
		writeLong(updateTime)
		writeLong(trackUpdateTime)
		writeInt(trackCount)
		writeString(commentThreadId)
		writeInt((if (highQuality) 1 else 0))
		writeInt(playCount)
		writeString(coverImgUrl)
		writeString(name)
		writeInt(id)
		writeInt(shareCount)
		writeString(coverImgIdStr)
		writeInt(commentCount)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Playlist> = object : Parcelable.Creator<Playlist> {
			override fun createFromParcel(source: Parcel): Playlist = Playlist(source)
			override fun newArray(size: Int): Array<Playlist?> = arrayOfNulls(size)
		}
	}
}

data class Track(
		@SerializedName("name") var name: String,
		@SerializedName("id") var id: Int,
		@SerializedName("pst") var pst: Int,
		@SerializedName("t") var t: Int,
		@SerializedName("ar") var ar: List<Ar>,
		//@SerializedName("alia") var alia: List<Any>,
		@SerializedName("pop") var pop: Int,
		@SerializedName("st") var st: Int,
		//@SerializedName("rt") var rt: Any,
		@SerializedName("fee") var fee: Int,
		@SerializedName("v") var v: Int,
		//@SerializedName("crbt") var crbt: Any,
		@SerializedName("cf") var cf: String,
		@SerializedName("al") var al: Al,
		@SerializedName("dt") var dt: Int,
		//@SerializedName("h") var h: H,
		//@SerializedName("m") var m: M,
		//@SerializedName("l") var l: L,
		//@SerializedName("a") var a: Any,
		@SerializedName("cd") var cd: String,
		@SerializedName("no") var no: Int,
		//@SerializedName("rtUrl") var rtUrl: Any,
		@SerializedName("ftype") var ftype: Int,
		@SerializedName("rtUrls") var rtUrls: List<Any>,
		@SerializedName("djId") var djId: Int,
		@SerializedName("copyright") var copyright: Int,
		@SerializedName("s_id") var sId: Int,
		@SerializedName("rtype") var rtype: Int,
		//@SerializedName("rurl") var rurl: Any,
		@SerializedName("mst") var mst: Int,
		@SerializedName("cp") var cp: Int,
		@SerializedName("mv") var mv: Int,
		@SerializedName("publishTime") var publishTime: Long
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readString(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.createTypedArrayList(Ar.CREATOR),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readString(),
			source.readParcelable<Al>(Al::class.java.classLoader),
			source.readInt(),
//			source.readParcelable<H>(H::class.java.classLoader),
//			source.readParcelable<M>(M::class.java.classLoader),
//			source.readParcelable<L>(L::class.java.classLoader),
			source.readString(),
			source.readInt(),
			source.readInt(),
			ArrayList<Any>().apply { source.readList(this, Any::class.java.classLoader) },
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readLong()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(name)
		writeInt(id)
		writeInt(pst)
		writeInt(t)
		writeTypedList(ar)
		writeInt(pop)
		writeInt(st)
		writeInt(fee)
		writeInt(v)
		writeString(cf)
		writeParcelable(al, 0)
		writeInt(dt)
		//writeParcelable(h, 0)
		//writeParcelable(m, 0)
		//writeParcelable(l, 0)
		writeString(cd)
		writeInt(no)
		writeInt(ftype)
		writeList(rtUrls)
		writeInt(djId)
		writeInt(copyright)
		writeInt(sId)
		writeInt(rtype)
		writeInt(mst)
		writeInt(cp)
		writeInt(mv)
		writeLong(publishTime)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Track> = object : Parcelable.Creator<Track> {
			override fun createFromParcel(source: Parcel): Track = Track(source)
			override fun newArray(size: Int): Array<Track?> = arrayOfNulls(size)
		}
	}
}

data class H(
		@SerializedName("br") var br: Int,
		@SerializedName("fid") var fid: Int,
		@SerializedName("size") var size: Int
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readInt(),
			source.readInt()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(br)
		writeInt(fid)
		writeInt(size)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<H> = object : Parcelable.Creator<H> {
			override fun createFromParcel(source: Parcel): H = H(source)
			override fun newArray(size: Int): Array<H?> = arrayOfNulls(size)
		}
	}
}

data class Ar(
		@SerializedName("id") var id: Int,
		@SerializedName("name") var name: String,
		@SerializedName("tns") var tns: List<Any>,
		@SerializedName("alias") var alias: List<Any>
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readString(),
			ArrayList<Any>().apply { source.readList(this, Any::class.java.classLoader) },
			ArrayList<Any>().apply { source.readList(this, Any::class.java.classLoader) }
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(id)
		writeString(name)
		writeList(tns)
		writeList(alias)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Ar> = object : Parcelable.Creator<Ar> {
			override fun createFromParcel(source: Parcel): Ar = Ar(source)
			override fun newArray(size: Int): Array<Ar?> = arrayOfNulls(size)
		}
	}
}

data class M(
		@SerializedName("br") var br: Int,
		@SerializedName("fid") var fid: Int,
		@SerializedName("size") var size: Int
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readInt(),
			source.readInt()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(br)
		writeInt(fid)
		writeInt(size)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<M> = object : Parcelable.Creator<M> {
			override fun createFromParcel(source: Parcel): M = M(source)
			override fun newArray(size: Int): Array<M?> = arrayOfNulls(size)
		}
	}
}

data class L(
		@SerializedName("br") var br: Int,
		@SerializedName("fid") var fid: Int,
		@SerializedName("size") var size: Int
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readInt(),
			source.readInt()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(br)
		writeInt(fid)
		writeInt(size)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<L> = object : Parcelable.Creator<L> {
			override fun createFromParcel(source: Parcel): L = L(source)
			override fun newArray(size: Int): Array<L?> = arrayOfNulls(size)
		}
	}
}

data class Al(
		@SerializedName("id") var id: Int,
		@SerializedName("name") var name: String,
		@SerializedName("picUrl") var picUrl: String,
		@SerializedName("tns") var tns: List<Any>,
		//@SerializedName("pic_str") var picStr: String,
		@SerializedName("pic") var pic: Long
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readString(),
			source.readString(),
			ArrayList<Any>().apply { source.readList(this, Any::class.java.classLoader) },
			//source.readString(),
			source.readLong()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(id)
		writeString(name)
		writeString(picUrl)
		writeList(tns)
		//writeString(picStr)
		writeLong(pic)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Al> = object : Parcelable.Creator<Al> {
			override fun createFromParcel(source: Parcel): Al = Al(source)
			override fun newArray(size: Int): Array<Al?> = arrayOfNulls(size)
		}
	}
}

data class TrackId(
		@SerializedName("id") var id: Int,
		@SerializedName("v") var v: Int,
		@SerializedName("lr") var lr: Int
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readInt(),
			source.readInt()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(id)
		writeInt(v)
		writeInt(lr)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<TrackId> = object : Parcelable.Creator<TrackId> {
			override fun createFromParcel(source: Parcel): TrackId = TrackId(source)
			override fun newArray(size: Int): Array<TrackId?> = arrayOfNulls(size)
		}
	}
}

data class Creator(
		@SerializedName("defaultAvatar") var defaultAvatar: Boolean,
		@SerializedName("province") var province: Int,
		@SerializedName("authStatus") var authStatus: Int,
		@SerializedName("followed") var followed: Boolean,
		@SerializedName("avatarUrl") var avatarUrl: String,
		@SerializedName("accountStatus") var accountStatus: Int,
		@SerializedName("gender") var gender: Int,
		@SerializedName("city") var city: Int,
		@SerializedName("birthday") var birthday: Long,
		@SerializedName("userId") var userId: Int,
		@SerializedName("userType") var userType: Int,
		@SerializedName("nickname") var nickname: String,
		@SerializedName("signature") var signature: String,
		@SerializedName("description") var description: String,
		@SerializedName("detailDescription") var detailDescription: String,
		@SerializedName("avatarImgId") var avatarImgId: Long,
		@SerializedName("backgroundImgId") var backgroundImgId: Long,
		@SerializedName("backgroundUrl") var backgroundUrl: String,
		@SerializedName("authority") var authority: Int,
		@SerializedName("mutual") var mutual: Boolean,
		//@SerializedName("expertTags") var expertTags: String,
		//@SerializedName("experts") var experts: String,
		@SerializedName("djStatus") var djStatus: Int,
		@SerializedName("vipType") var vipType: Int,
		//@SerializedName("remarkName") var remarkName: String,
		@SerializedName("avatarImgIdStr") var avatarImgIdStr: String,
		@SerializedName("backgroundImgIdStr") var backgroundImgIdStr: String
) : Parcelable {
	constructor(source: Parcel) : this(
			1 == source.readInt(),
			source.readInt(),
			source.readInt(),
			1 == source.readInt(),
			source.readString(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readLong(),
			source.readInt(),
			source.readInt(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readLong(),
			source.readLong(),
			source.readString(),
			source.readInt(),
			1 == source.readInt(),
			//source.readString(),
			//source.readString(),
			source.readInt(),
			source.readInt(),
			//source.readString(),
			source.readString(),
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt((if (defaultAvatar) 1 else 0))
		writeInt(province)
		writeInt(authStatus)
		writeInt((if (followed) 1 else 0))
		writeString(avatarUrl)
		writeInt(accountStatus)
		writeInt(gender)
		writeInt(city)
		writeLong(birthday)
		writeInt(userId)
		writeInt(userType)
		writeString(nickname)
		writeString(signature)
		writeString(description)
		writeString(detailDescription)
		writeLong(avatarImgId)
		writeLong(backgroundImgId)
		writeString(backgroundUrl)
		writeInt(authority)
		writeInt((if (mutual) 1 else 0))
		//writeString(expertTags)
		//writeString(experts)
		writeInt(djStatus)
		writeInt(vipType)
		//writeString(remarkName)
		writeString(avatarImgIdStr)
		writeString(backgroundImgIdStr)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Creator> = object : Parcelable.Creator<Creator> {
			override fun createFromParcel(source: Parcel): Creator = Creator(source)
			override fun newArray(size: Int): Array<Creator?> = arrayOfNulls(size)
		}
	}
}

data class Privilege(
		@SerializedName("id") var id: Int,
		@SerializedName("fee") var fee: Int,
		@SerializedName("payed") var payed: Int,
		@SerializedName("st") var st: Int,
		@SerializedName("pl") var pl: Int,
		@SerializedName("dl") var dl: Int,
		@SerializedName("sp") var sp: Int,
		@SerializedName("cp") var cp: Int,
		@SerializedName("subp") var subp: Int,
		@SerializedName("cs") var cs: Boolean,
		@SerializedName("maxbr") var maxbr: Int,
		@SerializedName("fl") var fl: Int,
		@SerializedName("toast") var toast: Boolean,
		@SerializedName("flag") var flag: Int
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			source.readInt(),
			1 == source.readInt(),
			source.readInt(),
			source.readInt(),
			1 == source.readInt(),
			source.readInt()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(id)
		writeInt(fee)
		writeInt(payed)
		writeInt(st)
		writeInt(pl)
		writeInt(dl)
		writeInt(sp)
		writeInt(cp)
		writeInt(subp)
		writeInt((if (cs) 1 else 0))
		writeInt(maxbr)
		writeInt(fl)
		writeInt((if (toast) 1 else 0))
		writeInt(flag)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Privilege> = object : Parcelable.Creator<Privilege> {
			override fun createFromParcel(source: Parcel): Privilege = Privilege(source)
			override fun newArray(size: Int): Array<Privilege?> = arrayOfNulls(size)
		}
	}
}
