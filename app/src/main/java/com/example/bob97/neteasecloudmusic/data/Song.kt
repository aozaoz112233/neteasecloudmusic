package com.example.bob97.neteasecloudmusic.data
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName


/**
 * Created by bob97 on 2018/1/5.
 */

data class Song(
		@SerializedName("data") var data: List<Data>,
		@SerializedName("code") var code: Int
){
	class Deserializer : ResponseDeserializable<Song> {
		override fun deserialize(content: String) = Gson().fromJson(content, Song::class.java)
	}
}

data class Data(
		@SerializedName("id") var id: Int,
		@SerializedName("url") var url: String,
		@SerializedName("br") var br: Int,
		@SerializedName("size") var size: Int,
		@SerializedName("md5") var md5: Any,
		@SerializedName("code") var code: Int,
		@SerializedName("expi") var expi: Int,
		@SerializedName("type") var type: Any,
		@SerializedName("gain") var gain: Int,
		@SerializedName("fee") var fee: Int,
		@SerializedName("uf") var uf: Any,
		@SerializedName("payed") var payed: Int,
		@SerializedName("flag") var flag: Int,
		@SerializedName("canExtend") var canExtend: Boolean
)