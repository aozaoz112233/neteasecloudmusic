package com.example.bob97.neteasecloudmusic.networkManager

import com.example.bob97.neteasecloudmusic.BuildConfig
import com.example.bob97.neteasecloudmusic.user.User
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by bob97 on 2018/7/18.
 */
class Net(){

    private val TIME_OUT:Long = 15
    private val BASE_URL = "http://10.25.130.136:3000/"
    var retrofit: Retrofit = initRetrofit()

//单例模式
    companion object {
        fun getInstance() = Holder.INSTANCE
        }
    private object Holder{
        val INSTANCE = Net()
    }
    private fun initRetrofit():Retrofit{
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        }
        else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
        }

        val client = OkHttpClient.Builder()
                .addInterceptor (interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(TIME_OUT,TimeUnit.SECONDS)
                .readTimeout(TIME_OUT,TimeUnit.SECONDS)
                .build()

        return Retrofit.Builder()
                .baseUrl(BASE_URL + "/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
    }

}