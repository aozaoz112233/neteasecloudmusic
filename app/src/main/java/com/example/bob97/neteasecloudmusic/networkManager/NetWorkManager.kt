package com.example.bob97.neteasecloudmusic.networkManager

import com.example.bob97.neteasecloudmusic.data.RankResult
import com.example.bob97.neteasecloudmusic.data.Song
import com.example.bob97.neteasecloudmusic.data.accounts
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.httpGet
import javax.xml.transform.Result

/**
 * Created by bob97 on 2017/12/28.
 */
class NetWorkManager {
    companion object {
        private fun host(): String {
            return "http://localhost:3000/"
        }
        private val phoneLogin = host() + "/login/cellphone?"

        private var cookies: String = ""
        private val rankURL = host() + "/top/list"
        //private val idURL = host() + "/music/url"
        fun setAuthCookies(cookies: String) {
            this.cookies = cookies
            FuelManager.instance.baseHeaders = mapOf("Cookie" to cookies)
        }


//        internal fun login(
//                phone: String,
//                pasword: String,
//                complete: (loginModel: accounts?, cookies: String, error: FuelError?) -> Unit)
//        {
//            FuelManager.instance
//                    .request(Method.GET, phoneLogin,
//                            listOf(Pair("phone", phone), Pair("password", pasword)))
//                    .responseObject(accounts.Deserializer()) {
//                        request, response, result ->
//             android.os.NetworkOnMainThreadExceptionandroid.os.NetworkOnMainThreadExceptionandroid.os.NetworkOnMainThreadException           when(result){
//                            is com.github.kittinunf.result.Result.Failure -> {
//                                complete(null,"", result.error)
//                            }
//                            is com.github.kittinunf.result.Result.Success -> {
//                                //获取Cookie信息
//                                val cookies = response.httpResponseHeaders
//                                        .filter { it.key.equals("Set-Cookie",true) }
//                                        .values
//                                        .first()
//                                        .map { it.substring(0,it.indexOf(';')+1) }
//                                        .reduce { l, r ->r+l  }
//                                val (data ,err) = result
//                                complete(data, cookies,null)
//                            }
//                        }android.os.NetworkOnMainThreadException
//
//                    }
//        }
//        internal fun getRank(
//          idx: String,
//          complete: (list: RankResult_2?, error: FuelError?) -> Unit
//        ){
//            FuelManager.instance
//                    .request(Method.GET, rankURL, listOf(Pair("idx",idx)))
//                    .responseObject(RankResult_2.Deserializer()){
//                        request, response, result ->
//                        when(result){
//                            is com.github.kittinunf.result.Result.Failure -> {
//                                complete(null,result.error)
//                            }
//                            is com.github.kittinunf.result.Result.Success ->{
//                                val(data,err) = result
//                                complete(data,null)
//                            }
//                        }
//                    }
//        }

//        fun findMusicById(
//                id: String,
//        complete: (song: Song?, error: FuelError?) -> Unit
//        ){
//            FuelManager.instance
//                    .request(Method.GET, idURL, listOf(Pair("id",id)))
//                    .responseObject(Song.Deserializer()){
//                        request, response, result ->
//                        print(result.component1())
//                        when(result){
//                            is com.github.kittinunf.result.Result.Failure -> {
//                                complete(null,result.error)
//                            }
//                            is com.github.kittinunf.result.Result.Success -> {
//                                val(data,err) = result
//                                complete(data!!,null)
//                            }
//                        }
//                    }
//        }
            fun findMusicById(id: String) :String{ return "http://music.163.com/song/media/outer/url?id=${id}.mp3"}


    }


}