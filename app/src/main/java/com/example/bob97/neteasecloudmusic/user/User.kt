package com.example.bob97.neteasecloudmusic.user

import android.accounts.Account
import android.content.Context
import android.content.SyncInfo
import com.example.bob97.neteasecloudmusic.networkManager.NetWorkManager
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Method

/**
 * Created by bob97 on 2017/12/28.
 */
class User private constructor() {
    var mContext: Context? = null
    val cookieKey = "COOKIE"
    val phoneKey = "PHONE"
    val nameKey = "NAME"
    val mottoKey = "MOTTO"
    val avatarUrlKey = "AVATAR"
    val nickNamekey = "NICK"
    var cookie = ""
    var phone = ""
    var motto = ""
    var avatarUrl = ""
    var name = ""
    var nickName = ""

    //单例
    companion object {
        private object Inner {
            val single = User()
        }

        fun get(): User {
            return Inner.single
        }
    }
    //登录与cookie保存
//    fun login(phone:String,pasword:String,completionHandler:(error:String?)->Unit){
//        val lastphone = loadInfo(phoneKey)
//        if (phone == lastphone)
//        {
//            NetWorkManager.setAuthCookies(loadInfo(cookieKey))
//            loadAll()
//            completionHandler(null)
//        }
//        else{
////             NetWorkManager.login(phone,pasword){
////                 data,cookies,err->
////                 if (err == null){
////                     saveInfo(nameKey,data?.account?.userName!!)
////                     saveInfo(mottoKey,data?.profile?.signature!!)
////                     saveInfo(avatarUrlKey,data?.profile?.avatarUrl!!)
////                     saveInfo(nickNamekey,data?.profile?.nickname!!)
////                     saveInfo(cookieKey,cookies)
////                     saveInfo(phoneKey,phone)
////                     NetWorkManager.setAuthCookies(cookies)
////                     loadAll()
////                     completionHandler(null)
//                 }
////                 else{
////                     completionHandler(err.toString())
////                 }
//             }



    //读取所有信息
//    fun loadAll(){
//        cookie = loadInfo(cookieKey)
//        phone = loadInfo(phoneKey)
//        name = loadInfo(nameKey)
//        motto = loadInfo(mottoKey)
//        avatarUrl = loadInfo(avatarUrlKey)
//        nickName = loadInfo(nickNamekey)
//    }
    //重置所有信息
//    fun restInfo(){
//        listOf<String>(cookieKey,phoneKey,mottoKey,nameKey,avatarUrlKey,nickNamekey)
//                .forEach { saveInfo(it,"") }
//    }
    //通过SharedPreferences保存
    private fun saveInfo(key: String,info: String){
        val preferences = mContext!!.getSharedPreferences("User",Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString(key,info)
        editor.commit()
    }
    //从SharedPreferences读取
    private fun loadInfo(key:String):String{
        val preferences = mContext!!.getSharedPreferences("User",Context.MODE_PRIVATE)
        val info = preferences.getString(key,"")
        return info
    }



}